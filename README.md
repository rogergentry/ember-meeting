# Ember Meeting

Ember Meeting is a audio conference interface to take meeting notes based on your agenda.

## About Ember Meeting
Ember Meeting was built to solve a problem. The problem of conference calls lasting too long. By adding an agenda and templates we can keep the call on track!